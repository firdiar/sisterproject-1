const express = require('express')
const app = express()
const port = process.env.PORT ||3000;

const database = require('./config/config.js');
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', (req, res) => {
	
	let ref = database.ref("test");
			
	ref.once('value').then(function(snapshot) {
			if(snapshot.val() != null){
				let valueTemp = snapshot.val();
				console.log(valueTemp);
				res.send({valueTemp});	
			}else{
				res.send(null);
			}
			
	});
			

})
app.get('/order', (req, res) => {
	
	let ref = database.ref("order");
			
	ref.once('value').then(function(snapshot) {
			if(snapshot.val() != null){
				res.send(snapshot.val());	
			}else{
				res.send(null);
			}
			
	});
			

})
app.get('/jadwal', (req, res) => {
	
	let ref = database.ref("jadwal");
			
	ref.once('value').then(function(snapshot) {
			if(snapshot.val() != null){
				res.send(snapshot.val());	
			}else{
				res.send(null);
			}
			
	});
			

})


app.listen(port, () => console.log(`Example app listening on port ${port}!`))